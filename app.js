const knex = require("knex")
const express = require("express")
const app = express()
let config = require('./knexfile.js')
let db = knex(config.development)
let jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const cookieParser = require('cookie-parser')
let Cookies = require( "cookies" );
const bearerToken = require('express-bearer-token');

let CreateAccount = require('./Middleware/CreateAccount')
let ConfirmRegistration = require('./Middleware/ConfirmRegistration')
let ActivateAccount = require('./Middleware/ActivateAccount')
let Login = require('./Middleware/Login')
let UserService = require('./Middleware/UserService')
let PictureService = require('./Middleware/PictureService')

app.use(express.json())// for parsing application/json 
app.use(express.urlencoded({ extended: true })) // for parsing application/x-www-form-urlencoded
app.post("/register", CreateAccount)
app.get("/confirm-registration", ConfirmRegistration) //sending a confirmation mail (this is a simulation to avoid nodemailer or express-mailer)
app.post("/activate-account", ActivateAccount)
app.post('/login', Login)
app.use(cookieParser())
app.use(bearerToken({
    cookie: {
        signed: true, 
        secret: 'MYSECRET',
        key: 'access_token' 
    }
}))
app.use('/api/users', UserService)
app.use('/api/pictures', PictureService)
app.use('/api/pictures/?user=', UserService)
app.use(express.static('public')) //Static file serving
app.listen(3615, function () {
    console.log("Server listening on port 3615")
})