const express = require("express")
const app = express()
const knex = require("knex")
let config = require('../knexfile.js')
let db = knex(config.development)
let jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')


module.exports = async function (req,res){
    try{
        let saltRounds = 10
        console.log(req.body['password'])
        let hash = bcrypt.hashSync(req.body['password'], saltRounds)
        console.log(hash)
        await db.into('User').update({is_active: true}).where({email: req.body.email})
        await db.into('User').update({password: hash}).where({email: req.body.email})
        console.log('this user is now active')
        res.redirect('login.html')
    }
    catch(e){
        res.sendStatus(400)
    }
}

