const knex = require("knex")
let config = require('../knexfile.js')
let db = knex(config.development)
let jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')
let Cookies = require( "cookies" )


module.exports = async function (req,res){
    try{ 
        await db.into('User').select('*').where({email: req.body['email']})
    }
    catch(e1){
        res.sendStatus(403)
    }
    try{
        await db.into('User').select('*').where({email: req.body.email}).andWhere({is_active: true})
    }
    catch(e2){
        res.sendStatus(403).send("ce compte n'est plus actif")
    }
    try{
        crypted_pwd = await db.into('User').select('password').where({email: req.body.email}).andWhere({is_active: true});
        console.log(crypted_pwd)
        parsed_pwd = crypted_pwd[0]['password']
        console.log(parsed_pwd)
        pwd = req.body['password']
        bcrypt.compareSync(pwd,parsed_pwd) === true
    }
    catch(e3){
        res.sendStatus(403).send("le mot de passe n'est pas valide")
    }
    console.log(req.body['password'])
    id_user = await db.into('User').select('id').where({email: req.body.email})
    is_admin = await db.into('User').select('is_admin').where({email:req.body.email})
    id = id_user[0]['id']
    console.log(id)
    let token = jwt.sign(id, "MYSECRET")
    console.log(token)
    new Cookies(req,res).set('access_token',token);
    if (is_admin === true){
        res.cookie(token,id, { path: '/api/users', expires: new Date(Date.now() + 600000), httpOnly: true });
        res.cookie(token,id, { path: '/api/pictures', expires: new Date(Date.now() + 600000), httpOnly: true });
    } else {
        res.cookie(token,id, { path: '/api/users/'+id, expires: new Date(Date.now() + 600000), httpOnly: true });
        res.cookie(token,id, { path: '/api/pictures/'+id, expires: new Date(Date.now() + 600000), httpOnly: true });
        res.cookie(token,id, { path: '/api/pictures/?user='+id, expires: new Date(Date.now() + 600000), httpOnly: true });
    }
    res.redirect('home.html')
}

