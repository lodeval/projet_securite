const express = require("express")
const app = express()
const knex = require("knex")
let config = require('../knexfile.js')
let db = knex(config.development)
let jwt = require('jsonwebtoken');

module.exports = async function (req, res){    
    try{
        await db.into('User').insert({email:req.body.email, fullname: req.body.fullname})
        let token = jwt.sign({email:req.body.email,fullname: req.body.fullname}, "MYSECRET") 
        console.log('localhost:3615/confirm-registration?token=' + token) //Synchronous Sign with default (HMAC SHA256)
        console.log(req.body.email)
        res.sendStatus(200)
    }
    catch(e){
        res.sendStatus(400)
    }
}

